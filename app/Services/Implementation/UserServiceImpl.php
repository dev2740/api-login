<?php
namespace App\Services\Implementation;

use  App\Services\Interfaces\IUserServiceInterface;
use  App\Models\User;
use  Illuminate\Support\Facades\Hash;

class UserServiceImpl implements IUserServiceInterface
{
    private $model;

    function __construct(){
        $this->model = new User();
    }

    function getUser(){
        //trae los datos que estan activos
        return $this->model->get();

        //trae todos los datos aunque este eliminados. pr
        //return $this->model->withTrashed()->get();
    }
    function getUserById(int $idUser)
    {
        return $this->model->find($idUser);
    }
    /**
     * crea un nuevo usuario en el sistema
     */
    function postUser(array $user)
    {
        $user['password'] = Hash::make($user['password']);
        $this->model->create($user);
    }
    function putUser(array $user, int $idUser){

        $user['password'] = Hash::make($user['password']);
        $this->model->where('id', $idUser)
                    ->first()
                    ->fill($user)
                    ->save();

    }
    function delUser(int $idUser)
    {
        $user = $this->model->find($idUser);

        if($user != null) {
            $user->delete();
        }
    }
    function restoreUser(int $idUser)
    {
        $user = $this->model->withTrashed()->find($idUser);

        if($user != null) {
            $user->restore();
        }
    }
}
