<?php
namespace App\Services\Interfaces;

interface IUserServiceInterface
{
    /**
     * @return array User
     */
    function getUser();
    /**
     * @param int $idUser
     * @return User
     */
    function getUserById(int $idUser);
    /**
     * @param array $user
     * @return void
     */
    function postUser(array $user);
    /**
     * @param array $user
     * @param int $idUser
     * @return void
     */
    function putUser(array $user, int $idUser);
    /**
     * @param int $idUser
     * @return boolean
     */
    function delUser(int $idUser);
    /**
     * @param int $idUser
     * @return boolean
     */
    function restoreUser(int $idUser);
}
