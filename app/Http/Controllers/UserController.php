<?php
//https://dev.to/meherulsust/how-to-build-a-jwt-authenticated-api-with-lumen-831-171o
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Implementation\UserServiceImpl;
use App\Validator\UserValidator;

class UserController extends Controller
{
    /**
     * @var UserServiceImpl
     */
    private $userService;

    /**
     * @var Rerquest
     */
    private $request;

    /**
     * @var UserValidator
     */
    private $validator;

    public function __construct(UserServiceImpl $UserService, Request $request, UserValidator $userValidator)
    {
        $this->userService = $UserService;
        $this->request = $request;
        $this->validator = $userValidator;
    }

    function createUser()
    {
        $validator = $this->validator->validate();

        if($validator->fails()){
            return response()->json(["status" => 422
                                    ,"massage" => "Error"
                                    ,"errors" => $validator->errors()], 422);
        }else{
            try {
                $this->userService->postUser($this->request->all());
                return response()->json(['success' => 'User Created successfully'], 201);
            }catch(\Throwable $e){
                return response()->json([
                    'error' => [
                        'description' => $e->getMessage()
                    ]
                ], 500);
            }
        }
    }

    function getListUser()
    {
        try {
            return response()->json(['users' => $this->userService->getUser()], 200);
        }catch(\Throwable $e){
            return response()->json([
                'error' => [
                    'description' => $e->getMessage()
                ]
            ], 500);
        }
    }

    function getUserById(int $idUser)
    {
        try {
            return response()->json($this->userService->getUserById($idUser), 200);
        }catch(\Throwable $e){
            return response()->json([
                'error' => [
                    'description' => $e->getMessage()
                ]
            ], 500);
        }
    }

    function putUser(int $idUser)
    {
        $validator = $this->validator->validate();

        if($validator->fails()){
            return response()->json(["status" => 422
                                    ,"massage" => "Error"
                                    ,"errors" => $validator->errors()], 422);
        }else{
            try {
                $this->userService->putUser($this->request->all(), $idUser);
                return response()->json(['success' => 'User Update successfully'], 202);
            }catch(\Throwable $e){
                return response()->json([
                    'error' => [
                        'description' => $e->getMessage()
                    ]
                ], 500);
            }
        }
    }

    function deleteUser(int $idUser)
    {
        try {
            $this->userService->delUser($idUser);
            return response()->json(['success' => 'User Delete successfully'], 202);
        }catch(\Throwable $e){
            return response()->json([
                'error' => [
                    'description' => $e->getMessage()
                ]
            ], 500);
        }
    }

    function restoreUser(int $idUser)
    {
        try {
            $this->userService->restoreUser($idUser);
            return response()->json(['success' => 'User Restore successfully'], 202);
        }catch(\Throwable $e){
            return response()->json([
                'error' => [
                    'description' => $e->getMessage()
                ]
            ], 500);
        }
    }
}
