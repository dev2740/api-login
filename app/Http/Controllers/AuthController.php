<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use  App\Models\User;
use App\Validator\LoginValidator;

class AuthController extends Controller
{
    /**
     * @var LoginValidator
     */
    private $loginValidator;

    public function __construct(LoginValidator $loginValidator)
    {
        $this->middleware('auth:api', ['except' => ['login', 'refresh', 'logout']]);
        $this->loginValidator = $loginValidator;
    }
    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
        try {
            $loginValidator = $this->loginValidator->validate();

            if($loginValidator->fails()){
                return response()->json(["status" => 422
                                        ,"massage" => "Error"
                                        ,"errors" => $loginValidator->errors()], 422);
            }else{
                $credentials = $request->only(['email', 'password']);

                if (! $token = Auth::attempt($credentials)) {
                    return response()->json(['message' => 'Unauthorized'], 401);
                }

                return $this->respondWithToken($token);
            }
        }catch(\Throwable $e){
            return response()->json([
                'error' => [
                    'description' => $e->getMessage()
                ]
            ], 500);
        }
    }

     /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        try {
            return response()->json(auth()->user());
        }catch(\Throwable $e){
            return response()->json([
                'error' => [
                    'description' => $e->getMessage()
                ]
            ], 500);
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            auth()->logout();
            return response()->json(['message' => 'Successfully logged out']);
        }catch(\Throwable $e){
            return response()->json([
                'error' => [
                    'description' => $e->getMessage()
                ]
            ], 500);
        }
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        try {
            return $this->respondWithToken(auth()->refresh());
        }catch(\Throwable $e){
            return response()->json([
                'error' => [
                    'description' => $e->getMessage()
                ]
            ], 500);
        }
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            //'user' => auth()->user(),
            'expires_in' => auth()->factory()->getTTL() * 60 * 24
        ]);
    }
}
