<?php

namespace App\Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginValidator
{
    /**
     * @var Request
     */

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function validate()
    {
        return Validator::make($this->request->all(), $this->rules(), $this->messages());
    }

    private function rules()
    {
        return [
            'email' => 'required|email|string'
            ,'password' => 'required|string'
        ];
    }

    private function messages()
    {
        return [
            'email.required' => 'Se requiere un email',
            'email.email' => 'Email no Valido',
            'email.string' => 'email String no Valido',
            'password.required' => 'Se requiere un password',
            'password.string' => 'password String no Valido',
        ];
    }
}
