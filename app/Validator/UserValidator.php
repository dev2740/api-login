<?php

namespace App\Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserValidator
{
    /**
     * @var Request
     */

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function validate()
    {
        return Validator::make($this->request->all(), $this->rules(), $this->messages());
    }

    private function rules()
    {
        return [
            'name' => 'required'
            ,'email' => 'required|email|unique:users,email,'. $this->request->id
            ,'password' => 'required'
            ,'confirm_password' => 'required|same:password'
        ];
    }

    private function messages()
    {
        return [
            'name.required' => 'Se requiere un name',
            'email.required' => 'Se requiere un email',
            'email.email' => 'Email no Valido',
            'email.unique' => 'Email ya existente.',
            'password.required' => 'Se requiere un password',
            'confirm_password.required' => 'Se requiere un confirm_password',
            'confirm_password.same' => 'La password de confirmación y la password deben coincidir.',
        ];
    }
}
