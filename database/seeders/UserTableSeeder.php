<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Miller Ulloa',
            'email' => 'yugo147@gmail.com',
            'password' => Hash::make('1a2b3c')
            //'password' => Hash::make('12345678')
        ]);
    }
}
