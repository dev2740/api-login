<?php
//https://dev.to/meherulsust/how-to-build-a-jwt-authenticated-api-with-lumen-831-171o
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    echo "<center> Welcome </center>";
});

$router->get('/version', function () use ($router) {
    return $router->app->version();
});

$router->group(["prefix" => "/v1"], function () use ($router){
    $router->group(["prefix" => "/auth"], function () use ($router){
        /*$router->post('/register', 'UserController@createUser');
        $router->get('/list', 'UserController@getListUser');
        $router->get('/{idUser}', 'UserController@getUserById');
        $router->put('/{idUser}', 'UserController@putUser');
        $router->delete('/{idUser}', 'UserController@deleteUser');
        $router->get('/{idUser}/restore', 'UserController@restoreUser');*/
        $router->post('/login', 'AuthController@login');
        $router->post('/logout', 'AuthController@logout');
        $router->post('/refresh', 'AuthController@refresh');
        $router->post('/user-profile', 'AuthController@me');
    });

    // $router->group(["prefix" => "/user"], function () use ($router){
    $router->group(["prefix" => "/user", "middleware" => "auth"], function () use ($router){
        $router->post('/register', 'UserController@createUser');
        $router->get('/list', 'UserController@getListUser');
        $router->get('/{idUser}', 'UserController@getUserById');
        $router->put('/{idUser}', 'UserController@putUser');
        $router->delete('/{idUser}', 'UserController@deleteUser');
        $router->get('/{idUser}/restore', 'UserController@restoreUser');
    });

});


